﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float BorderRight;
    public float BorderLeft;
    public float BorderTop;
    public float BorderBottom;

    public int NumberOfPointsToWin;

    public bool Paused;
    public bool GameOver;

    public Text ScoreText;
    public Text OverlayText;

    private int scorePlayer1;
    private int scorePlayer2;

    public void UpdateScore()
    {
        ScoreText.text = scorePlayer1 + " | " + scorePlayer2;

        if (scorePlayer1 > NumberOfPointsToWin)
        {
            Win("Player 1");
        }
        else if (scorePlayer2 > NumberOfPointsToWin)
        {
            Win("Player 2");
        }
    }

    public void PointForPlayer1()
    {
        scorePlayer1++;
        UpdateScore();
    }

    public void PointForPlayer2()
    {
        scorePlayer2++;
        UpdateScore();
    }

    private void Win(string winner)
    {
        GameOver = true;
        Paused = true;
        OverlayText.text = winner + " wins!";
        OverlayText.gameObject.SetActive(true);
    }

    private void Pause()
    {
        Paused = true;
        OverlayText.text = "Paused";
        OverlayText.gameObject.SetActive(true);
    }

    private void UnPause()
    {
        Paused = false;
        OverlayText.gameObject.SetActive(false);
    }

    private void ResetGame()
    {
        scorePlayer1 = 0;
        scorePlayer2 = 0;
        GameOver = false;
        UnPause();
    }

    private void Update()
    {
        if (GameOver)
        {
            if (Input.anyKeyDown)
            {
                ResetGame();
            }
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Paused)
            {
                UnPause();
            }
            else
            {
                Pause();
            }
        }
    }

}
