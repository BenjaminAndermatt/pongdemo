﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMover : MonoBehaviour
{
    public float Speed;

    private GameManager game;

    private Vector3 currentMovementDirection;

    private void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    public void MoveUp()
    {
        if (transform.position.y < game.BorderTop)
        {
            currentMovementDirection = Vector3.up;
        }
    }

    public void MoveDown()
    {
        if (transform.position.y > game.BorderBottom)
        {
            currentMovementDirection = Vector3.down;
        }
    }

    private void FixedUpdate()
    {
        transform.position += currentMovementDirection * Speed * Time.fixedDeltaTime;
        currentMovementDirection = Vector3.zero;
    }
}
