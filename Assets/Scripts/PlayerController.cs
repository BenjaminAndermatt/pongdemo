﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public KeyCode UpKey;
    public KeyCode DownKey;

    public PaddleMover paddle;

    void Update()
    {
        if (Input.GetKey(UpKey))
        {
            paddle.MoveUp();
        }
        if (Input.GetKey(DownKey))
        {
            paddle.MoveDown();
        }
    }
}
