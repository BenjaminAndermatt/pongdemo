﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Vector3 MoveDirection;
    private Vector3 lastVelocity;
    private bool stopped;

    public float Speed;

    private Rigidbody rigidbody;

    private GameManager game;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        RandomDirection();

        game = FindObjectOfType<GameManager>();
    }

    public void RandomDirection()
    {
        float randomAngle = Random.Range(30f, -30f);
        if (Random.value > 0.5f)
        {
            randomAngle = Random.Range(210f, -150f);
        }

        transform.eulerAngles = new Vector3(0f, 0f, randomAngle);
        rigidbody.velocity = transform.right * Speed;
    }

    private void ResetBall()
    {
        transform.position = Vector3.zero;
        RandomDirection();
    }

    private void MoveBall()
    {
        if (stopped)
        {
            rigidbody.velocity = lastVelocity;
            stopped = false;
        }

        rigidbody.velocity = rigidbody.velocity.normalized * Speed;
    }

    private void StopBall()
    {
        stopped = true;
        lastVelocity = rigidbody.velocity;
        rigidbody.velocity = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if(game.Paused == false)
        {
            MoveBall();
        }
        else if (stopped == false)
        {
            StopBall();
        }

        if (transform.position.x > game.BorderRight)
        {
            game.PointForPlayer1();
            ResetBall();
        }
        if (transform.position.x < game.BorderLeft)
        {
            game.PointForPlayer2();
            ResetBall();
        }
    }    
}
