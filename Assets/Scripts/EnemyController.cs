﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private PaddleMover paddle;
    private Transform ball;
    private float halfHeight;

    void Start()
    {
        paddle = GetComponent<PaddleMover>();
        ball = FindObjectOfType<Ball>().transform;
        halfHeight = GetComponent<BoxCollider>().bounds.extents.y;
    }

    private void FixedUpdate()
    {
        if (ball.position.y > transform.position.y + halfHeight)
        {
            paddle.MoveUp();
        }
        else if (ball.position.y < transform.position.y - halfHeight)
        {
            paddle.MoveDown();
        }
    }
}
